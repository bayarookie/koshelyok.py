from flask import Flask, session, redirect, url_for, request, render_template, make_response, g
from markupsafe import escape
from datetime import datetime, date, timedelta
import sqlite3, os, traceback, sys, random, glob, hashlib, re, getopt

app = Flask(__name__)
exiting = False
#app.config['UPLOAD_FOLDER'] = "/tmp"
# set the secret key. keep this really secret:
# python -c 'import os; print(os.urandom(16))'
app.secret_key = b'\xd0\xaaQ\xaeeD\xfcI\xbfs\x0e\x0fA\x8b\x93\xc8'
DATABASE = 'koshelyok.db'

def get_db():
    try:
        db = getattr(g, '_database', None)
        if db is None:
            db = g._database = sqlite3.connect(DATABASE)
        return db
    except sqlite3.Error as er:
        print(er)

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

def sql_selall(s):
    try:
        with app.app_context():
            c = get_db().cursor()
            c.execute(s)
            return c.fetchall()
    except sqlite3.Error as er:
        print(er)
        
def exec_script(sqlfile):
    try:
        with app.app_context():
            c = get_db().cursor()
            sql_file = open(sqlfile, 'r')
            sql_as_string = sql_file.read()
            c.executescript(sql_as_string)
    except sqlite3.Error as er:
        print('SQLite error: %s' % (' '.join(er.args)))
        print("Exception class is: ", er.__class__)
        print('SQLite traceback: ')
        exc_type, exc_value, exc_tb = sys.exc_info()
        print(traceback.format_exception(exc_type, exc_value, exc_tb))

def hex_code_colors():
    r = hex(random.randrange(0,255))
    g = hex(random.randrange(0,255))
    b = hex(random.randrange(0,255))
    r = r[2:]
    g = g[2:]
    b = b[2:]
    if len(r)<2: r = "0" + r
    if len(g)<2: g = "0" + g
    if len(b)<2: b = "0" + b
    z = r + g + b
    return "#" + z.upper()

@app.route('/', methods=['GET', 'POST'])
def index():
    if not 'username' in session:
        session['url'] = url_for('index')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        # фильтры из формы
        mo = int(request.form['mo'])
        dt = escape(request.form['dt'])
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
        #dt = datetime.strptime(dt, '%Y-%m-%d').strftime("%Y-%m-%d")
        #fr = datetime.strptime(fr, '%Y-%m-%d').strftime("%Y-%m-%d")
        #to = datetime.strptime(to, '%Y-%m-%d').strftime("%Y-%m-%d")
        #ik = int(request.form['ik'])
        # https://www.w3schools.com/howto/howto_js_autocomplete.asp
        # "Название конторы | id"
        a = escape(request.form['ik']).split(' | ')
        if a[-1].isdecimal():
            # если есть, взять id из строки
            ik = int(a[-1])
        else:
            ik = -1
        ig = int(request.form['ig'])
        ib = int(request.form['ib'])
        iw = int(request.form['iw'])
        iu = int(request.form['iu'])
    else:
        if 'mo' in session:
            # фильтры из сессии
            mo = session['mo']
            dt = session['dt']
            fr = session['fr']
            to = session['to']
            ik = session['ik']
            ig = session['ig']
            ib = session['ib']
            iw = session['iw']
            iu = session['iu']
        else:
            # фильтры по умолчанию
            c.execute("SELECT MAX(op_date) FROM money")
            r = c.fetchone()
            if r is not None:
                dt = datetime.strptime(r[0], '%Y-%m-%d')
            else:
                dt = datetime.today()
            year = dt.year
            month = dt.month - 1
            if month < 1:
                month = month + 12
                year = year - 1
            fr = date(year, month, 1)
            mo = 1
            dt = dt.strftime("%Y-%m-%d")
            fr = fr.strftime("%Y-%m-%d")
            to = dt
            ik = -1
            ig = -1
            ib = -1
            iw = -1
            iu = -1

# фильтры в сессию
    session['mo'] = mo
    session['dt'] = dt
    session['fr'] = fr
    session['to'] = to
    session['ik'] = ik
    session['ig'] = ig
    session['ib'] = ib
    session['iw'] = iw
    session['iu'] = iu

# сортировка
    c.execute('SELECT order_by FROM money_order WHERE id=?', (mo, ))
    r = c.fetchone()
    if r is not None:
        s = r[0]
    else:
        s = ''
    if s != '':
        order = f'ORDER BY {s}'
    else:
        order = 'ORDER BY money.op_date'

# фильтр
    filtr = ''
    if ik >= 0: filtr += f' AND money.servs_id={ik}'
    if ig >= 0: filtr += f' AND servs.grups_id={ig}'
    if ib >= 0: filtr += f' AND grups.bgrup_id={ib}'
    if iw >= 0: filtr += f' AND money.walls_id={iw}'
    if iu >= 0: filtr += f' AND walls.users_id={iu}'

# состояние на начало по каждому кошельку
    where = f'WHERE money.op_date<"{fr}"' + filtr

    c.execute("""SELECT MAX(op_date), SUM(op_summ), walls.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN walls ON money.walls_id=walls.id """
    + where +
    " GROUP BY walls_id")
    b1 = c.fetchall()

# состояние на начало
    c.execute("""SELECT MAX(op_date), SUM(op_summ)
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN walls ON money.walls_id=walls.id """
    + where)
    b2 = c.fetchall()

# пополнения и списания за период
    where = f'WHERE money.op_date>="{fr}" AND money.op_date<="{to}"' + filtr

    c.execute("SELECT ROW_NUMBER () OVER ("
    + order +
    """) rnum,
    money.id, op_date,
    CASE WHEN op_summ>=0 THEN op_summ ELSE '' END,
    CASE WHEN op_summ<0 THEN op_summ ELSE '' END,
    CASE WHEN servs.comment='' THEN servs.name ELSE servs.comment END,
    grups.name, walls.name, users.name, money.comment
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    LEFT JOIN walls ON money.walls_id=walls.id
    LEFT JOIN users ON walls.users_id=users.id """
    + where)
    r = c.fetchall()

# сумма +пополнений -списаний за период
    c.execute("""SELECT SUM(deb.op_summ), SUM(cre.op_summ), SUM(money.op_summ), walls.name
    FROM money
    LEFT JOIN money deb ON money.id=deb.id AND deb.op_summ>0
    LEFT JOIN money cre ON money.id=cre.id AND cre.op_summ<0
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN walls ON money.walls_id=walls.id """
    + where +
    " GROUP BY money.walls_id")
    d = c.fetchall()

# итого по каждому кошельку
    where = f'WHERE money.op_date<="{to}"' + filtr

    c.execute("""SELECT MAX(op_date), SUM(op_summ), walls.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN walls ON money.walls_id=walls.id """
    + where +
    " GROUP BY money.walls_id")
    e = c.fetchall()

# итого
    c.execute("""SELECT MAX(op_date), SUM(op_summ)
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN walls ON money.walls_id=walls.id """
    + where)
    t = c.fetchall()

# сортировка
    c.execute("SELECT id, name || ' - ' || comment FROM money_order")
    m = c.fetchall()
    
# фильтры списки
    c.execute("SELECT id, name || ' - ' || comment FROM servs ORDER by name")
    k = c.fetchall()
    c.execute("SELECT id, name || ' - ' || comment FROM grups")
    g = c.fetchall()
    c.execute("SELECT id, name || ' - ' || comment FROM bgrup")
    b = c.fetchall()
    c.execute("SELECT id, name || ' - ' || comment FROM walls")
    w = c.fetchall()
    c.execute("SELECT id, name || ' - ' || comment FROM users")
    u = c.fetchall()

# нижний колонтитул
    l = 'Пользователь зашёл как ' + escape(session['username']) + ' ' + escape(session['full_acc'])

# фильтр по конторе
    if ik < 0:
        sr = ''
    else:
        c.execute("""SELECT servs.id,
        CASE WHEN bgrup.name IS NULL
        THEN servs.name || ' - ' || servs.comment
        ELSE servs.name || ' - ' || servs.comment || ' - ' || bgrup.name
        END
        FROM servs
        LEFT JOIN grups ON servs.grups_id=grups.id
        LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
        WHERE servs.id=?""", (ik, ))
        s = c.fetchone()
        if s is not None:
            sr = s[1] + ' | ' + str(s[0])
        else:
            sr = ''

    return render_template("money.html",
        begina=b1, beginb=b2, rows=r, dvizh=d, summa=e, itogo=t,
        dt=dt, mo=mo, fr=fr, to=to, ik=ik, ig=ig, ib=ib, iw=iw, iu=iu,
        monor=m, servs=k, serv=sr, grups=g, bgrup=b, walls=w, users=u, bottom=l)

@app.route('/money_edit', methods=['GET', 'POST'])
def money_edit():
    if not 'username' in session:
        session['url'] = url_for('money_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        dt = escape(request.form['dt'])
        sm = float(request.form['sm'])
        # https://www.w3schools.com/howto/howto_js_autocomplete.asp
        # "Название конторы | id"
        a = escape(request.form['se']).split(' | ')
        if a[-1].isdecimal():
            # если есть, взять id из строки
            se = int(a[-1])
        else:
            # если нет, то добавить эту контору в БД и взять id из БД
            se_cm = escape(request.form['se_cm'])
            se_gr = int(request.form['se_gr'])
            c.execute("""INSERT INTO servs (name, comment, grups_id)
            VALUES(?, ?, ?)""", (a[0], se_cm, se_gr))
            se = c.lastrowid
            
        wa = int(request.form['wa'])
        cm = escape(request.form['cm'])
        if id < 0:
            c.execute("""INSERT INTO money (op_date, op_summ, servs_id, walls_id, comment) 
            VALUES(?, ?, ?, ?, ?)""", (dt, sm, se, wa, cm))
        else:
            c.execute("""UPDATE money
            SET op_date=?, op_summ=?, servs_id=?, walls_id=?, comment=?
            WHERE id=?""", (dt, sm, se, wa, cm, id))
        con.commit()
        session['dt'] = dt
        if 'to' in session:
            d = datetime.strptime(dt, '%Y-%m-%d')
            t = datetime.strptime(session['to'], '%Y-%m-%d')
            if d > t:
                session['to'] = dt
        return redirect(url_for('index'))

    #if 'dt' in session: dt = session['dt']
    #if dt == None:      dt = date.today()
    dt = date.today()
    
# список контор
    c.execute("""SELECT servs.id, 
        CASE WHEN bgrup.name IS NULL
        THEN servs.name || ' - ' || servs.comment
        ELSE servs.name || ' - ' || servs.comment || ' - ' || bgrup.name
        END
        FROM servs
        LEFT JOIN grups ON servs.grups_id=grups.id
        LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
        ORDER by bgrup.name, servs.name""")
    k = c.fetchall()

# список подгрупп    
    c.execute("SELECT id, name || ' - ' || comment FROM grups")
    g = c.fetchall()
    
# список кошельков
    c.execute("SELECT id, name || ' - ' || comment FROM walls")
    w = c.fetchall()

    id = int(request.args.get('id'))
    if id >= 0:
        # редактирование записи
        c.execute("SELECT * FROM money WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            c.execute("""SELECT servs.id,
            CASE WHEN bgrup.name IS NULL
            THEN servs.name || ' - ' || servs.comment
            ELSE servs.name || ' - ' || servs.comment || ' - ' || bgrup.name
            END
            FROM servs
            LEFT JOIN grups ON servs.grups_id=grups.id
            LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
            WHERE servs.id=?""", (r[3], ))
            s = c.fetchone()
            if s is not None:
                sr = s[1] + ' | ' + str(s[0])
            else:
                sr = ''
            con.close()
            return render_template("money_edit.html", id=id, dt=r[1],
                sm=r[2], se=r[3], wa=r[4], cm=r[5], servs=k, walls=w, serv=sr, grups=g)

    # добавление записи
    con.close()
    return render_template("money_edit.html", id=-1, dt=dt,
        sm=0.00, se=-1, wa=session['walls_id'], cm='', servs=k, walls=w, serv='', grups=g)


@app.route('/money2', methods=['GET', 'POST'])
def money2():
    if not 'username' in session:
        session['url'] = url_for('servs')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        mn = request.form['mn']
        ik = request.form['ik']
        ig = request.form['ig']
        ib = request.form['ib']
        iu = request.form['iu']
    else:
        mn = request.args.get('mn')
        ik = request.args.get('ik')
        ig = request.args.get('ig')
        ib = request.args.get('ib')
        iu = request.args.get('iu')

    if ik is None: ik = -1
    else: ik = int(ik)
    if ig is None: ig = -1
    else: ig = int(ig)
    if ib is None: ib = -1
    else: ib = int(ib)
    if iu is None: iu = -1
    else: iu = int(iu)
    print(mn, ik, ig, ib, iu)

# фильтр
    if mn is None:
        where = 'WHERE TRUE'
    else:
        where = f'WHERE strftime("%Y-%m", op_date)="{mn}"'
    if ik >= 0: where += f' AND money.servs_id={ik}'
    if ig >= 0: where += f' AND servs.grups_id={ig}'
    if ib >= 0: where += f' AND grups.bgrup_id={ib}'
    if iu >= 0: where += f' AND walls.users_id={iu}'

#
    c.execute("""SELECT ROW_NUMBER () OVER (
    ORDER BY money.op_date
    ) rnum,
    money.id, op_date,
    CASE WHEN op_summ>=0 THEN op_summ ELSE '' END,
    CASE WHEN op_summ<0 THEN op_summ ELSE '' END,
    CASE WHEN servs.comment='' THEN servs.name ELSE servs.comment END,
    grups.name, walls.name, users.name, money.comment
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    LEFT JOIN walls ON money.walls_id=walls.id
    LEFT JOIN users ON walls.users_id=users.id
    """ + where)
    r = c.fetchall()
    return render_template("money2.html", rows=r, ik=ik)    
        
@app.route('/money2_edit', methods=['GET', 'POST'])
def money2_edit():
    if not 'username' in session:
        session['url'] = url_for('servs')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        ik = int(request.form['ik'])
        id = int(request.form['id'])
        dt = escape(request.form['dt'])
        sm = float(request.form['sm'])
        se = int(request.form['se'])
        wa = int(request.form['wa'])
        cm = escape(request.form['cm'])
        if id < 0:
            c.execute("""INSERT INTO money (op_date, op_summ, servs_id, walls_id, comment) 
            VALUES(?, ?, ?, ?, ?)""", (dt, sm, se, wa, cm))
        else:
            c.execute("""UPDATE money
            SET op_date=?, op_summ=?, servs_id=?, walls_id=?, comment=?
            WHERE id=?""", (dt, sm, se, wa, cm, id))
        con.commit()
        session['dt'] = dt
        if 'to' in session:
            d = datetime.strptime(dt, '%Y-%m-%d')
            t = datetime.strptime(session['to'], '%Y-%m-%d')
            if d > t:
                session['to'] = dt
        return redirect(url_for('money2'), code=307)

    dt = date.today()
    
# список контор
    c.execute("""SELECT servs.id,
        CASE WHEN bgrup.name IS NULL
        THEN servs.name || ' - ' || servs.comment
        ELSE servs.name || ' - ' || servs.comment || ' - ' || bgrup.name
        END
        FROM servs
        LEFT JOIN grups ON servs.grups_id=grups.id
        LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
        ORDER by bgrup.name, servs.name""")
    k = c.fetchall()
    
# список кошельков
    c.execute("SELECT id, name || ' - ' || comment FROM walls")
    w = c.fetchall()
    
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute("SELECT * from money WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            return render_template("money2_edit.html", ik=r[3], id=id, dt=r[1],
                sm=r[2], se=r[3], wa=r[4], cm=r[5], servs=k, walls=w)

    return render_template("money2_edit.html", ik=-1, id=-1, dt=dt,
        sm=0.00, se=-1, wa=session['walls_id'], cm='', servs=k, walls=w)

@app.route('/servs', methods=['GET', 'POST'])
def servs():
    if not 'username' in session:
        session['url'] = url_for('servs')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("""SELECT servs.id, servs.name, servs.comment, grups.name, count(money.id)
    FROM servs
    LEFT JOIN grups ON servs.grups_id = grups.id
    LEFT JOIN money ON money.servs_id = servs.id
    GROUP BY servs.id 
    ORDER BY grups.name, servs.name""")
    r = c.fetchall()
    c.execute("SELECT id, name || ' - ' || comment FROM grups")
    g = c.fetchall()
    return render_template("servs.html", rows=r, grups=g)

@app.route('/servs_edit', methods=['GET', 'POST'])
def servs_edit():
    if not 'username' in session:
        session['url'] = url_for('servs_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        nm = escape(request.form['nm'])
        cm = escape(request.form['cm'])
        gr = int(request.form['gr'])
        if id < 0:
            c.execute("""INSERT INTO servs (name, comment, grups_id)
            VALUES(?, ?, ?)""", (nm, cm, gr))
        else:
            c.execute("""UPDATE servs
            SET name=?, comment=?, grups_id=?
            WHERE id=?""", (nm, cm, gr, id))
        con.commit()
        return redirect(url_for('servs'))
    
    c.execute("SELECT id, name || ' - ' || comment FROM grups")
    g = c.fetchall()
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute("SELECT * from servs WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            return render_template("servs_edit.html", id=id, nm=r[1], cm=r[2], gr=r[3], grups=g)
    return render_template("servs_edit.html", id=-1, nm='', cm='', gr=-1, grups=g)

@app.route('/grups', methods=['GET', 'POST'])
def grups():
    if not 'username' in session:
        session['url'] = url_for('grups')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("""SELECT grups.id, grups.name, grups.comment, bgrup.name
    FROM grups
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    ORDER by bgrup.name, grups.name""")
    r = c.fetchall()
    return render_template("grups.html", rows=r)

@app.route('/grups_edit', methods=['GET', 'POST'])
def grups_edit():
    if not 'username' in session:
        session['url'] = url_for('grups_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        nm = escape(request.form['nm'])
        cm = escape(request.form['cm'])
        bg = int(request.form['bg'])
        if id < 0:
            c.execute("""INSERT INTO grups (name, comment, bgrup_id)
            VALUES(?, ?, ?)""", (nm, cm, bg))
        else:
            c.execute("""UPDATE grups
            SET name=?, comment=?, bgrup_id=?
            WHERE id=?""", (nm, cm, bg, id))
        con.commit()
        return redirect(url_for('grups'))
    
# список групп    
    c.execute("SELECT id, name || ' - ' || comment FROM bgrup")
    b = c.fetchall()
    
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute("SELECT * from grups WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            return render_template("grups_edit.html", id=id, nm=r[1], cm=r[2], bg=r[3], bgrup=b)
    return render_template("grups_edit.html", id=-1, nm='', cm='', bg=-1, bgrup=b)

@app.route('/bgrup', methods=['GET', 'POST'])
def bgrup():
    if not 'username' in session:
        session['url'] = url_for('bgrup')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("SELECT * FROM bgrup ORDER by name")
    r = c.fetchall()
    return render_template("bgrup.html", rows=r)

@app.route('/bgrup_edit', methods=['GET', 'POST'])
def bgrup_edit():
    if not 'username' in session:
        session['url'] = url_for('bgrup_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        nm = escape(request.form['nm'])
        cm = escape(request.form['cm'])
        if id < 0:
            c.execute("""INSERT INTO bgrup (name, comment)
            VALUES(?, ?)""", (nm, cm))
        else:
            c.execute("""UPDATE bgrup
            SET name=?, comment=?
            WHERE id=?""", (nm, cm, id))
        con.commit()
        return redirect(url_for('bgrup'))
    
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute("SELECT * from bgrup WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            return render_template("bgrup_edit.html", id=id, nm=r[1], cm=r[2])
    return render_template("bgrup_edit.html", id=-1, nm='', cm='')

@app.route('/walls', methods=['GET', 'POST'])
def walls():
    if not 'username' in session:
        session['url'] = url_for('walls')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("""SELECT walls.id, walls.name, walls.comment, users.name
    FROM walls
    LEFT JOIN users ON walls.users_id=users.id""")
    r = c.fetchall()
    return render_template("walls.html", rows=r)

@app.route('/walls_edit', methods=['GET', 'POST'])
def walls_edit():
    if not 'username' in session:
        session['url'] = url_for('walls_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        nm = escape(request.form['nm'])
        cm = escape(request.form['cm'])
        us = int(request.form['us'])
        if id < 0:
            c.execute("""INSERT INTO walls (name, comment, users_id)
            VALUES(?, ?, ?)""", (nm, cm, us))
        else:
            c.execute("""UPDATE walls
            SET name=?, comment=?, users_id=?
            WHERE id=?""", (nm, cm, us, id))
        con.commit()
        return redirect(url_for('walls'))
    
# список пользователей    
    c.execute("SELECT id, name || ' - ' || comment FROM users")
    b = c.fetchall()
    
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute("SELECT * from walls WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            return render_template("walls_edit.html", id=id, nm=r[1], cm=r[2], us=r[3], users=b)
    return render_template("walls_edit.html", id=-1, nm='', cm='', us=-1, users=b)

@app.route('/users', methods=['GET', 'POST'])
def users():
    if not 'username' in session:
        session['url'] = url_for('users')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("""SELECT users.id, users.username, users.password, users.salt, users.name, users.comment, walls.name
    FROM users
    LEFT JOIN walls ON users.walls_id=walls.id""")
    r = c.fetchall()
    return render_template("users.html", rows=r)

@app.route('/users_edit', methods=['GET', 'POST'])
def users_edit():
    if not 'username' in session:
        session['url'] = url_for('users_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        un = escape(request.form['un'])
        pw = escape(request.form['pw'])
        nm = escape(request.form['nm'])
        cm = escape(request.form['cm'])
        wa = int(request.form['wa'])
        # зашифровать пароль
        sa = os.urandom(16)
        pw = hashlib.pbkdf2_hmac('sha256', pw.encode('utf-8'), sa, 100000)
        pw = sqlite3.Binary(pw)
        
        if id < 0:
            c.execute("""INSERT INTO users (username, password, salt, name, comment, walls_id) 
            VALUES(?, ?, ?, ?, ?)""", (un, pw, sa, nm, cm, wa))
        else:
            c.execute("""UPDATE users
            SET username=?, password=?, salt=?, name=?, comment=?, walls_id=?
            WHERE id=?""", (un, pw, sa, nm, cm, wa, id))
        con.commit()
        return redirect(url_for('users'))

# список кошельков    
    c.execute("SELECT id, name || ' - ' || comment FROM walls")
    b = c.fetchall()
    
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute(f"SELECT id, username, password, name, comment, walls_id from users WHERE id={id}")
        r = c.fetchone()
        if r is not None:
            return render_template("users_edit.html", id=id, un=r[1], pw='', nm=r[3], cm=r[4], wa=r[5], walls=b)
    return render_template("users_edit.html", id=-1, un='', pw='',  nm='', cm='', wa=-1, walls=b)

@app.route('/money_order', methods=['GET', 'POST'])
def money_order():
    if not 'username' in session:
        session['url'] = url_for('money_order')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("SELECT * FROM money_order")
    r = c.fetchall()
    return render_template("money_order.html", rows=r)

@app.route('/money_order_edit', methods=['GET', 'POST'])
def money_order_edit():
    if not 'username' in session:
        session['url'] = url_for('money_order_edit')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        id = int(request.form['id'])
        nm = escape(request.form['nm'])
        ob = escape(request.form['ob'])
        cm = escape(request.form['cm'])
        if id < 0:
            c.execute("""INSERT INTO money_order (name, order_by, comment)
            VALUES(?, ?, ?)""", (nm, ob, cm))
        else:
            c.execute("""UPDATE money_order
            SET name=?, order_by=?, comment=?
            WHERE id=?""", (nm, ob, cm, id))
        con.commit()
        return redirect(url_for('money_order'))
    
    id = int(request.args.get('id'))
    if id >= 0:
        c.execute("SELECT * from money_order WHERE id=?", (id, ))
        r = c.fetchone()
        if r is not None:
            con.close()
            return render_template("money_order_edit.html", id=id, nm=r[1], ob=r[2], cm=r[3])
    con.close()
    return render_template("money_order_edit.html", id=-1, nm='', ob='', cm='')

@app.route('/report01', methods=['GET', 'POST'])
def report01():
    if not 'username' in session:
        session['url'] = url_for('report01')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = dt.strftime("%Y-%m-%d")
    
# месяца    
    c.execute("""SELECT DISTINCT strftime('%Y-%m', op_date) as mo
    FROM money
    WHERE op_date>=? AND op_date<=?
    ORDER BY mo""", (fr, to))
    d = c.fetchall()
    
# группы    
    c.execute("""SELECT DISTINCT bgrup_id, bgrup.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    WHERE op_date>=? AND op_date<=?
    ORDER BY bgrup_id""", (fr, to))
    e = c.fetchall()

# помесячно     
    g = []
    for r in e:
        h = {}
        bgrup_id = r[0]
        h['bgrup_id'] = r[0]
        h['name'] = r[1]
        for t in d:
            h[t[0]] = 0.00
        c.execute("""SELECT strftime('%Y-%m', op_date) as mo, ABS(SUM(op_summ)) as summ
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        LEFT JOIN grups ON servs.grups_id=grups.id
        WHERE op_date>=? AND op_date<=? AND bgrup_id=?
        GROUP by mo""", (fr, to, bgrup_id))
        f = c.fetchall()
        for v in f:
            h[v[0]] = round(v[1], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report01.html", fr=fr, to=to, columns=d, rows=g)
    
@app.route('/report02', methods=['GET', 'POST'])
def report02():
    if not 'username' in session:
        session['url'] = url_for('report02')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = dt.strftime("%Y-%m-%d")
        
# месяца
    c.execute("""SELECT DISTINCT strftime('%Y-%m', op_date) as mo
    FROM money
    WHERE op_date>=? AND op_date<=?
    ORDER BY mo""", (fr, to))
    d = c.fetchall()

# группы    
    c.execute("""SELECT DISTINCT bgrup_id, bgrup.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    WHERE op_date>=? AND op_date<=?
    ORDER BY bgrup_id""", (fr, to))
    e = c.fetchall()

# помесячно    
    g = []
    for r in e:
        h = {}
        bgrup_id = r[0]
        h['bgrup_id'] = r[0]
        h['name'] = r[1]
        for t in d:
            h[t[0]] = 0.00
        c.execute("""SELECT strftime('%Y-%m', op_date) as mo, ABS(SUM(op_summ)) as summ
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        LEFT JOIN grups ON servs.grups_id=grups.id
        WHERE op_date>=? AND op_date<=? AND bgrup_id=?
        GROUP by mo""", (fr, to, bgrup_id))
        f = c.fetchall()
        for v in f:
            h[v[0]] = round(v[1], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report02.html", fr=fr, to=to, columns=d, rows=g)
    
@app.route('/report03', methods=['GET', 'POST'])
def report03():
    if not 'username' in session:
        session['url'] = url_for('report03')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = date(dt.year, dt.month, 1) - timedelta(days=1)
        to = to.strftime("%Y-%m-%d")
    
# количество месяцев    
    c.execute("""SELECT COUNT(DISTINCT strftime('%Y-%m', op_date))
    FROM money
    WHERE op_date>=? AND op_date<=?""", (fr, to))
    d = c.fetchone()
    if d is not None:
        m = d[0]
    else:
        m = '1'

# подгруппы   
    c.execute("""SELECT DISTINCT grups_id, grups.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    WHERE op_date>=? AND op_date<=?
    ORDER BY grups_id""", (fr, to))
    e = c.fetchall()
    
# средний за месяц   
    g = []
    for r in e:
        h = {}
        grups_id = r[0]
        h['name'] = r[1]
        c.execute("""SELECT ABS(SUM(op_summ)) / ?
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        WHERE op_date>=? AND op_date<=? AND grups_id=?""", (m, fr, to, grups_id))
        f = c.fetchall()
        for v in f:
            h['average'] = round(v[0], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report03.html", fr=fr, to=to, rows=g)

@app.route('/report04', methods=['GET', 'POST'])
def report04():
    if not 'username' in session:
        session['url'] = url_for('report04')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = date(dt.year, dt.month, 1) - timedelta(days=1)
        to = to.strftime("%Y-%m-%d")

# количество месяцев
    c.execute("""SELECT COUNT(DISTINCT strftime('%Y-%m', op_date))
    FROM money
    WHERE op_date>=? AND op_date<=?""", (fr, to))
    d = c.fetchone()
    if d is not None:
        m = d[0]
    else:
        m = '1'

# группы
    c.execute("""SELECT DISTINCT bgrup_id, bgrup.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    WHERE op_date>=? AND op_date<=?
    ORDER BY bgrup_id""", (fr, to))
    e = c.fetchall()

# средний за месяц
    g = []
    for r in e:
        h = {}
        bgrup_id = r[0]
        h['name'] = r[1]
        c.execute("""SELECT ABS(SUM(op_summ)) / ?
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        LEFT JOIN grups ON servs.grups_id=grups.id
        WHERE op_date>=? AND op_date<=? AND bgrup_id=?""", (m, fr, to, bgrup_id))
        f = c.fetchall()
        for v in f:
            h['average'] = round(v[0], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report04.html", fr=fr, to=to, rows=g)

@app.route('/report05', methods=['GET', 'POST'])
def report05():
    if not 'username' in session:
        session['url'] = url_for('report05')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
        iu = int(request.form['iu'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = dt.strftime("%Y-%m-%d")
        iu = session['full_acc']

# пользователи
    c.execute("SELECT id, name || ' - ' || comment FROM users")
    u = c.fetchall()

# месяца
    c.execute("""SELECT DISTINCT strftime('%Y-%m', op_date) as mo
    FROM money
    WHERE op_date>=? AND op_date<=?
    ORDER BY mo""", (fr, to))
    d = c.fetchall()

# группы
    c.execute("""SELECT DISTINCT bgrup_id, bgrup.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
    LEFT JOIN walls ON money.walls_id=walls.id
    WHERE op_date>=? AND op_date<=? AND users_id=?
    ORDER BY bgrup_id""", (fr, to, iu))
    e = c.fetchall()

# помесячно
    g = []
    for r in e:
        h = {}
        bgrup_id = r[0]
        h['bgrup_id'] = r[0]
        h['name'] = r[1]
        for t in d:
            h[t[0]] = 0.00
        c.execute("""SELECT strftime('%Y-%m', op_date) as mo, ABS(SUM(op_summ)) as summ
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        LEFT JOIN grups ON servs.grups_id=grups.id
        LEFT JOIN walls ON money.walls_id=walls.id
        WHERE op_date>=? AND op_date<=? AND bgrup_id=? AND users_id=?
        GROUP by mo""", (fr, to, bgrup_id, iu))
        f = c.fetchall()
        for v in f:
            h[v[0]] = round(v[1], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report05.html", fr=fr, to=to, iu=iu, users=u, columns=d, rows=g)

@app.route('/report06', methods=['GET', 'POST'])
def report06():
    if not 'username' in session:
        session['url'] = url_for('report06')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
        ig = int(request.form['ig'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = dt.strftime("%Y-%m-%d")
        ig = 3

# группы
    c.execute("SELECT id, name FROM bgrup")
    u = c.fetchall()

# месяца
    c.execute("""SELECT DISTINCT strftime('%Y-%m', op_date) as mo
    FROM money
    WHERE op_date>=? AND op_date<=?
    ORDER BY mo""", (fr, to))
    d = c.fetchall()

# конторы
    c.execute("""SELECT DISTINCT servs_id, servs.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    WHERE op_date>=? AND op_date<=? AND bgrup_id=?
    ORDER BY servs.name""", (fr, to, ig))
    e = c.fetchall()

# помесячно
    g = []
    for r in e:
        h = {}
        servs_id = r[0]
        h['name'] = r[1]
        for t in d:
            h[t[0]] = 0.00
        c.execute("""SELECT strftime('%Y-%m', op_date) as mo, ABS(SUM(op_summ)) as summ
        FROM money
        WHERE op_date>=? AND op_date<=? AND servs_id=?
        GROUP by mo""", (fr, to, servs_id))
        f = c.fetchall()
        for v in f:
            h[v[0]] = round(v[1], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report06.html", fr=fr, to=to, ig=ig, bgrup=u, columns=d, rows=g)

@app.route('/report07', methods=['GET', 'POST'])
def report07():
    if not 'username' in session:
        session['url'] = url_for('report07')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        fr = escape(request.form['fr'])
        to = escape(request.form['to'])
        ib = int(request.form['ib'])
    else:
        c.execute("SELECT MAX(op_date) FROM money")
        r = c.fetchone()
        if r is not None:
            dt = datetime.strptime(r[0], '%Y-%m-%d')
        else:
            dt = datetime.today()
        year = dt.year
        month = dt.month - 6
        if month < 1:
            month = month + 12
            year = year - 1
        fr = date(year, month, 1).strftime("%Y-%m-%d")
        to = dt.strftime("%Y-%m-%d")
        ib = 3

# группы
    c.execute("SELECT id, name FROM bgrup")
    u = c.fetchall()

# месяца
    c.execute("""SELECT DISTINCT strftime('%Y-%m', op_date) as mo
    FROM money
    WHERE op_date>=? AND op_date<=?
    ORDER BY mo""", (fr, to))
    d = c.fetchall()

# подгруппы
    c.execute("""SELECT DISTINCT grups_id, grups.name
    FROM money
    LEFT JOIN servs ON money.servs_id=servs.id
    LEFT JOIN grups ON servs.grups_id=grups.id
    WHERE op_date>=? AND op_date<=? AND bgrup_id=?
    ORDER BY servs.name""", (fr, to, ib))
    e = c.fetchall()

# помесячно
    g = []
    for r in e:
        h = {}
        grups_id = r[0]
        h['grups_id'] = r[0]
        h['name'] = r[1]
        for t in d:
            h[t[0]] = 0.00
        c.execute("""SELECT strftime('%Y-%m', op_date) as mo, ABS(SUM(op_summ)) as summ
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        WHERE op_date>=? AND op_date<=? AND grups_id=?
        GROUP by mo""", (fr, to, grups_id))
        f = c.fetchall()
        for v in f:
            h[v[0]] = round(v[1], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    if len(e) > 1:
        h = {}
        h['grups_id'] = -1
        h['name'] = "Итого"
        for t in d:
            h[t[0]] = 0.00
        c.execute("""SELECT strftime('%Y-%m', op_date) as mo, ABS(SUM(op_summ)) as summ
        FROM money
        LEFT JOIN servs ON money.servs_id=servs.id
        LEFT JOIN grups ON servs.grups_id=grups.id
        WHERE op_date>=? AND op_date<=? AND bgrup_id=?
        GROUP by mo""", (fr, to, ib))
        f = c.fetchall()
        for v in f:
            h[v[0]] = round(v[1], 2)
        h['color'] = hex_code_colors()
        g.append(h)
    return render_template("report07.html", fr=fr, to=to, ib=ib, bgrup=u, columns=d, rows=g)

@app.route('/import_form')
def import_form():
    if not 'username' in session:
        session['url'] = url_for('import_form')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("SELECT id, name || ' - ' || comment FROM walls")
    w = c.fetchall()
    return render_template("import_form.html", wa=session['walls_id'], walls=w)

@app.route('/import_form2', methods=['GET', 'POST'])
def import_form2():
    if not 'username' in session:
        session['url'] = url_for('import_form2')
        return redirect(url_for('login'))
    if request.method == 'POST':
        c = get_db().cursor()
        file = request.files['bankstate']
        if file.filename == '':
            print('Нет выбранного файла')
            return redirect(url_for('import_form'))
        f = file.read().decode("utf-8")
        r = []
        #for l in f.splitlines():
        for l in f.split("\n"):
            h = {}
            m = re.findall(r'[0-2][0-9]:[0-5][0-9]', l)
            if m:
                s = l.strip()
# 20.02.2021 06:30          Пенсия ПФР                                               +11 770,55          28 628,46
# 19.02.2021 08:09          PRODUKTY OT TITANA                                            752,30         16 857,91
# 22.01.2021 07:42         Пенсия ПФР                                           +11 770,55          68 951,36
# 20.01.2021 07:01         WWW.TGK-14.COM                                           269,65          57 180,81
                # дата
                h['dt'] = datetime.strptime(s[0:10], '%d.%m.%Y').strftime("%Y-%m-%d")
                
                # время
                h['ti'] = s[11:16]
                
                # остаток
                i = s.rfind('  ')
                t = s[i:].replace(' ', '').replace(',', '.')
                h['os'] = float(t)
                
                # сумма
                s = s[:i].strip()
                i = s.rfind('  ')
                t = s[i:].replace(' ', '').replace(',', '.')
                if t.find('+')<0: t = '-' + t
                h['sm'] = float(t)
                
                # название
                h['nm'] = s[17:i].strip()
                
                # поиск конторы по базе
                c.execute("SELECT id FROM servs WHERE name LIKE ?", ('%'+h['nm']+'%', ))
                d = c.fetchall()
                if d:
                    h['se'] = d[0][0]
                else:
                    h['se'] = -1
                    
                # поиск операции по базе
                c.execute("SELECT id FROM money WHERE op_date=? AND op_summ=?", (h['dt'], h['sm']))
                e = c.fetchall()
                if e:
                    h['ck'] = False
                else:
                    h['ck'] = True
                    
                r.append(h)
        wa = int(request.form['wa'])
        c.execute("SELECT id, name || ' - ' || comment FROM walls")
        w = c.fetchall()
        c.execute("""SELECT servs.id, 
        CASE WHEN bgrup.name IS NULL
        THEN servs.name || ' - ' || servs.comment
        ELSE servs.name || ' - ' || servs.comment || ' - ' || bgrup.name
        END
        FROM servs
        LEFT JOIN grups ON servs.grups_id=grups.id
        LEFT JOIN bgrup ON grups.bgrup_id=bgrup.id
        ORDER by bgrup.name, servs.name""")
        k = c.fetchall()
        return render_template("import_form2.html", rows=r, wa=wa, walls=w, servs=k)

    return redirect(url_for('import_form'))
    
@app.route('/import_form3', methods=['GET', 'POST'])
def import_form3():
    if not 'username' in session:
        session['url'] = url_for('import_form3')
        return redirect(url_for('login'))
    con = get_db()
    c = con.cursor()
    if request.method == 'POST':
        wa = escape(request.form['wa'])
        ck = request.form.getlist('ck')
        for a in ck:
            h = a.split(';')
            if h[2]=='-1':
                c.execute("SELECT id FROM servs WHERE name LIKE ?", ('%'+h[3]+'%', ))
                d = c.fetchall()
                if d:
                    h[2] = d[0][0]
                else:
                    h[2] = -1
            if h[2]==-1:
                c.execute("INSERT INTO servs (name, comment) VALUES (?, ?)", (h[3], ''))
                h[2] = c.lastrowid
            c.execute("""INSERT INTO money (op_date, op_summ, servs_id, walls_id, comment) 
            VALUES(?, ?, ?, ?, '')""", (h[0], h[1], h[2], wa))
        con.commit()    
    return redirect(url_for('index'))
    
@app.route('/backup')
def backup():
    if not 'username' in session:
        session['url'] = url_for('backup')
        return redirect(url_for('login'))
    con = get_db()
    t = ''
    for s in con.iterdump():
        t += s + '\n'
    r = make_response(t)
    r.headers['Content-Type'] = 'text/plain;charset=UTF-8'
    f = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    r.headers['Content-Disposition'] = f'attachment;filename=koshelyok-{f}.sql'
    return r

@app.route('/db_structure')
def db_structure():
    if not 'username' in session:
        session['url'] = url_for('db_structure')
        return redirect(url_for('login'))
    c = get_db().cursor()
    c.execute("SELECT sql FROM sqlite_master WHERE type='table';")
    d = c.fetchall()
    t = '<pre>'
    for e in d:
        for f in e:
            t += f + '\n'
    t += '</pre>'
    return t
        
@app.route('/db', methods=['GET', 'POST'])
def db():
    if not 'username' in session:
        session['url'] = url_for('db')
        return redirect(url_for('login'))
    c = get_db().cursor()
    if request.method == 'POST':
        sc = request.form['sc']
        c.executescript(sc)
    c.execute('SELECT * from sqlite_master where type= "table"')
    r = c.fetchall()
    return render_template("db.html", rows=r)
    
@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        c = get_db().cursor()
        u = escape(request.form['username'])
        p = escape(request.form['password'])
        c.execute('SELECT salt FROM users WHERE username=?', (u, ))
        d = c.fetchone()
        if d:
            sa = d[0]
            if sa != '': # если есть секретный ключ
                sa = sqlite3.Binary(sa)
                p = hashlib.pbkdf2_hmac('sha256', p.encode('utf-8'), sa, 100000)
        c.execute('SELECT walls_id, id FROM users WHERE username=? AND password=?', (u, p))
        d = c.fetchone()
        if d:
            session['username'] = u
            session['walls_id'] = d[0] # кошелёк по умолчанию
            session['full_acc'] = d[1] # доступ?
            if 'url' in session:
                return redirect(session['url'])
            return redirect(url_for('index'))
    # если запущен локально, то полный доступ    
    if request.host == 'localhost:5000':
        session['username'] = 'localhost'
        session['walls_id'] = 2 # кошелёк по умолчанию
        session['full_acc'] = 1 # полный доступ
        return redirect(url_for('index'))
    return render_template("login.html")

@app.route('/logout')
def logout():
    # удалить из сессии имя пользователя, если оно там есть
    #session.pop('username', None)
    session.clear()
    return redirect(url_for('index'))

@app.route('/shutdown', methods=['GET'])
def shutdown():
    global exiting
    exiting = True
    return '<h1>Сервер выключен...</h1>'

@app.teardown_request
def teardown(exception):
    if exiting:
        os._exit(0)

@app.route('/style', methods=['GET', 'POST'])
def style():
    if request.cookies.get('cssid'):
        s = "style{}.css".format(request.cookies.get('cssid'))
        res = make_response(s)
        res.set_cookie('cssid', s, max_age=60*60*24*60)
    else:
        res = make_response("style{}.css".format(request.cookies.get('cssid')))
    return res

if __name__ == '__main__':
    t = sql_selall('SELECT name from sqlite_master where type= "table"')
    # если бд пустая, создать таблицы из файла
    if 'money' not in str(t):
        fl = glob.glob('*.sql')
        # последний файл (максимальное время создания)
        fn = max(fl, key=os.path.getctime)
        exec_script(fn)
    # входящие параметры
    host = '127.0.0.1'
    port = 5000
    try:
        args, vals = getopt.getopt(sys.argv[1:],"hdt:p:",["help", "debug", "host=", "port="])
    except getopt.GetoptError:
        print('Использовать так: server.py -d -t 127.0.0.1 -p 8000')
        sys.exit(2)
    for arg, val in args:
        if arg in ("-h", "--help"):
            print("""параметры: 
    -d или --debug
    -t или --host 127.0.0.1
    -p или --port 8080""")
            sys.exit()
        elif arg in ("-d", "--debug"):
            print('режим разработчика вкл')
            app.debug = True
        elif arg in ("-t", "--host"):
            print('хост ' + val)
            host = val
        elif arg in ("-p", "--port"):
            print('порт ' + val)
            port = int(val)
    app.run(host=host, port=port)
