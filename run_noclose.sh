#!/bin/sh
tty -s;
if [ "0" = "$?" ]; then
python3 server.py -d -p 5000 &
(sleep 1 && xdg-open http://localhost:5000)
else
/usr/bin/konsole --noclose -e sh -c "python3 server.py -d -p 5000" &
(sleep 1 && xdg-open http://localhost:5000)
fi
