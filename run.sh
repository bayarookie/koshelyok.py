#!/bin/sh
tty -s;
if [ "0" -eq "$?" ]; then
python3 server.py &
(sleep 1 && xdg-open http://localhost:5000)
else
/usr/bin/konsole -e sh -c "python3 server.py" &
(sleep 1 && xdg-open http://localhost:5000)
fi
