CREATE TABLE bgrup (
  id integer PRIMARY KEY,
  name TEXT,
  comment TEXT
)
CREATE TABLE grups (
  id integer PRIMARY KEY,
  name TEXT,
  comment TEXT,
  bgrup_id integer
)
CREATE TABLE money (
  id INTEGER PRIMARY KEY,
  op_date TEXT,
  op_summ REAL,
  servs_id INTEGER,
  walls_id INTEGER,
  comment TEXT
)
CREATE TABLE money_order (
  id INTEGER PRIMARY KEY,
  name TEXT,
  order_by TEXT,
  comment TEXT
)
CREATE TABLE servs (
  id INTEGER PRIMARY KEY,
  name TEXT,
  comment TEXT,
  grups_id INTEGER
)
CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  username TEXT,
  password BLOB,
  salt BLOB,
  name TEXT,
  comment TEXT,
  walls_id INTEGER,
  style_id INTEGER
)
CREATE TABLE walls (
  id INTEGER PRIMARY KEY,
  name TEXT,
  comment TEXT,
  users_id INTEGER
)
